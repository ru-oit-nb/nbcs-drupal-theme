jQuery(document).ready( function() {
	
	/* Configurable Variables */
	// Image to use instead of the logo when in fluid layout
	var mobileLogoImage = "images/header-oit-horizontal.png";
	
	/* Insert mobile logo */
	var imgPath = Drupal.settings.basePath + Drupal.settings.pathToTheme.pathToTheme + "/" + mobileLogoImage;
	var mobileLogo = '<img id="logo-mobile" alt="small site logo" src="'+imgPath+'" />';
	jQuery(".logo-img a").append(mobileLogo);

	/* Auto-rotate homepage quicktabs */
	var rotateSpeed = 90000;

	var featureRotate = function() {
		var active_tab = jQuery('ul.quicktabs-style-nostyle').find('li.active');
		var next_tab = active_tab.next();

		if(next_tab.length){
			next_tab.find('a').trigger('click');
		} else {
			/* remove the following line if you don't want it to cycle */
			jQuery('ul.quicktabs-style-nostyle').find('li.first a').trigger('click');
		}

		featureTimeout = setTimeout(featureRotate, rotateSpeed);

		return false;
	};

	
	var featureTimeout = setTimeout(featureRotate, rotateSpeed);
	
	/* disable auto-rotate when the mouse is over the quicktabs div */
	jQuery("#quicktabs-ddblock_categories").bind("mouseenter", function() {
		clearTimeout(featureTimeout);
		featureTimeout = false;
		return false;
	}).bind("mouseleave", function() {
		featureTimeout = setTimeout(featureRotate, rotateSpeed);
		return false;
	});

	/* Remove all height/width attributes for <img> tags. This allows us to scale images properly with the responsive theme. */
	jQuery('div#region-content').find('input,img,span,p,table').each(function(){
		jQuery(this).removeAttr('width');
		jQuery(this).removeAttr('height');
		jQuery(this).css('width','');
		jQuery(this).css('height','');
//		jQuery(this).attr('style','');
//		jQuery(this).removeAttr('style');
	});

    /* auto resize iframes */
    jQuery('iframe.nbcs-autoresize').load(function() {
        this.height = (this.contentWindow.document.body.scrollHeight) + "px";
        this.width = (this.contentWindow.document.body.scrollWidth) + "px";
    });
    
    
	/* override hover events for touch devices. we want click instead */
	/*
	jQuery('a').on('click touchend', function(e) {
		jQuery(this).click();
	});
	*/
	/*jQuery('a').on('click touchend', function(e) {
		var el = jQuery(this);
		var link = el.attr('href');
		window.location = link;
	});
	*/
	/* detect touch devices */
	/*
	if (!("ontouchstart" in document.documentElement)) {
		document.documentElement.className += " no-touch";
	}
	*/
	
});