<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */
 
 
 /* Search Form in Top Bar */
 
function nbcs_form_alter(&$form, &$form_state, $form_id) {
	if ($form_id == 'search_block_form') {
		
		// Show this text in the box by default
		$placeholder_text = "Search this site";
		
		$form['search_block_form']['#title_display'] = 'invisible'; // Hide the label
		$form['search_block_form']['#default_value'] = t($placeholder_text); // HTML5 show "Search" in the box to avoid needing a label.
		$form['actions']['#attributes']['class'][] = 'element-invisible'; // hide the submit button

		// Add extra attributes to the text box
		$form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = '{$placeholder_text}';}";
		$form['search_block_form']['#attributes']['onfocus'] = "if (this.value == '{$placeholder_text}') {this.value = '';}";
		
		// Prevent user from searching the default text
		$form['#attributes']['onsubmit'] = "if(this.search_block_form.value=='{$placeholder_text}'){ alert('Please enter a search'); return false; }";

		// Alternative (HTML5) placeholder attribute instead of using the javascript
		$form['search_block_form']['#attributes']['placeholder'] = t($placeholder_text);
	}
} 

function nbcs_preprocess_html(&$variables) {

	// doctype 
	$variables['doctype'] = '<!DOCTYPE html>' . "\n";
	$variables['rdf'] = new stdClass;
	$variables['rdf']->version = '';
	$variables['rdf']->namespaces = '';
	$variables['rdf']->profile = '';

	// pass the path to this theme (/sites/all/themes/nbcs, usually) to javascript
	global $theme_path;
	drupal_add_js(array('pathToTheme' => array('pathToTheme' => $theme_path)), 'setting');
	
	// include js/script.js in every site page
	drupal_add_js(drupal_get_path('theme', 'nbcs') . '/js/script.js', array( 
		'scope' => 'header', 
		'weight' => '15' 
	));
}

function nbcs_preprocess_node(&$variables) {
	
	$variables['readmore'] = t('<span class="readmore-inline"><a href="!title">Read More</a></span>',
		array('!title' => $variables['node_url'])
	);
}

?>

